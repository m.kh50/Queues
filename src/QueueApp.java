
public class QueueApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SortedQueue<Integer> q = new SortedQueue<>();
		q.enqueue(750);
		q.enqueue(500);
		q.enqueue(250);
		System.out.println(q.first());
		q.dequeue();
		System.out.println(q.first());
		q.dequeue();
		System.out.println(q.first());
		
		System.out.println(q.size());
	}

}

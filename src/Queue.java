
public class Queue<T extends Comparable<T>> {

	Node<T> node;
	
	public Queue() {
		node = null;
	}
	
	public void enqueue(T data) {
		Node<T> temp = new Node<>();
		temp.data = data;
		temp.next = node;
		node = temp;
	}
	
	public boolean isEmpty() {
		return node == null;
				
	}
	
	public void dequeue() {
		if(!isEmpty()) this.node = dequeue(this.node);
		
	}
	
	private final Node<T> dequeue(Node<T> node) {
				
		if(node.next == null) return null;
		
		node.next =	dequeue(node.next);
		
		return node;
	}
	
	public T first() {
		return first(node);
	}
	
	private final T first(Node<T> node) {
		if(node == null) return null;
		
		if(node.next == null) return node.data;
		
		else return first(node.next);
		
	}
	
	public int size() {
		return size(node);
	}
	
	private final int size(Node<T> node) {
		if(node == null) return 0;
		
		if(node.next == null) return 1;
		
		else return (1 + size(node.next));
	}
		
}

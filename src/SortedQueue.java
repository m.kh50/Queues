
public class SortedQueue<T extends Comparable<T>> {

	Node<T> node;
	
	public SortedQueue() {
		node = null;
	}
	
	public void enqueue(T data) {
		this.node = enqueue(data, this.node);
	}

	private final Node<T> enqueue(T data, Node<T> n) {
		if(n == null || data.compareTo(n.data) < 0) {
			Node<T> temp = new Node<>();
			temp.data = data;
			temp.next = n;
			return temp;
		}
		n.next = enqueue(data, n.next);
		return n;
	}

	
	public boolean isEmpty() {
		return node == null;
				
	}
	

	public int size() {
		return isEmpty() ? 0 : size(node);
	}
	
	private final int size(Node<T> node) {
		
		return node.next == null ? 1 : (1 + size(node.next));
				
	}
	
	public T first() {
		return isEmpty() ? null : node.data;
	}
	
	public T dequeue() {
		
		if(isEmpty()) 
			return null;
		
		T temp = node.data;
		node = node.next;
		return temp;
		
	}
		
}


